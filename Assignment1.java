package day11;
import java.util.Scanner;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.ZoneOffset;
public class Assignment1 {
	enum Holidays{
		MONDAY{
			@Override
			int execute(int perday) {
				return perday;
			}
		},
		TUESDAY{
			@Override
			int execute(int perday) {
				return perday;
			}
		},
		WEDNESDAY{
			@Override
			int execute(int perday) {
				return perday;
			}
		},
		THURSDAY{
			@Override
			int execute(int perday) {
				return perday;
			}
		},
		FRIDAY{
			@Override
			int execute(int perday) {
				return perday;
			}
		},
		SATURDAY{
			@Override
			int execute(int perday) {
				return 0;
			}
		},
		SUNDAY{
			@Override
			int execute(int perday) {
				return 0;
			}
		};
		abstract int execute(int perday);
	}
	enum Govtholidays{
		JANUARY1,JANUARY26,AUGUST15;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the hours required for transportation ");
		int thours=sc.nextInt();
		System.out.println("enter date");
		int dt=sc.nextInt();
		System.out.println("enter month");
		int mt=sc.nextInt();
		System.out.println("enter year");
		int yr=sc.nextInt();
		LocalDate ld=LocalDate.of(yr, mt, dt);
		LocalDateTime lt=LocalDateTime.now(ZoneOffset.UTC);
		System.out.println(lt);

		int ini=0;
		int start_time=6;
		int rem=thours;
		//System.out.println("THE HOUR "+lt.getHour());
		int perday=24-lt.getHour();
		if(perday>=12) {
			perday=12;
		}
		
		boolean fl=false;
		while(ini<thours) {
			DayOfWeek w=ld.getDayOfWeek();
			Month m=ld.getMonth();
			int day=ld.getDayOfMonth();
			String w1=w.toString();
			String h=m.toString()+String.valueOf(day);
			Holidays op=Holidays.valueOf(w1);
			boolean govt_holiday=false;
			for(Govtholidays g:Govtholidays.values()) {//Check Whether It is a Goverment Holiday
				if(g.name().equals(h)) {
					govt_holiday=true;
					break;
				}
			}
			if(govt_holiday) {
				ld=ld.plusDays(1);
				continue;
			}
			if(!fl) {
				ini+=op.execute(perday);
				perday=12;
				fl=true;	
			}
			else {
				ini+=op.execute(perday);
			}
			if(rem>12) {
			rem-=perday;
			}
			if(ini>=thours) {
				break;
			}
			ld=ld.plusDays(1);
		}
		//getdata1();
		System.out.println("THE PACKAGE WILL BE DELIVERED ON "+ld.getDayOfMonth()+" : "+ld.getMonth()+" BY "+(rem+6)+" Hours");
	}

	
}